<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loans extends CI_Controller {


    public function index()
    {
        /*Load twig libray*/
        $this->load->library('twig');

        $this->load->model('loan_model');

        $loans = $this->loan_model->allLive();

        $this->twig->display('loans', array('loans' => $loans));
    }
}