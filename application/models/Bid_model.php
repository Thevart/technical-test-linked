<?php
/**
 * Created by PhpStorm.
 * User: thevart
 * Date: 30/01/16
 * Time: 22:08
 */

class Bid_model extends MY_Model {

    public $table = 'bid'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key

    protected  $id;
    protected  $loan_id;
    protected  $amount;
    protected  $rate;
    protected  $accepted;
    protected  $date;

    public function __construct()
    {
        parent::__construct();
    }

    public function all()
    {
        $query = $this->db->get('bid');
        return $query->result();
    }
}