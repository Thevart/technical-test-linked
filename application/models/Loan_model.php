<?php

use LinkedTest\Model\Loan;

class Loan_model extends MY_Model {

    public $table = 'loan';
    public $primary_key = 'id';

    /*Describe OneToMany relationship*/
    public $has_many = array('bids' => array('Bid_model','loan_id','id'));

    public $return_type  = 'loan';


    public function __construct()
    {
        parent::__construct();
    }

    public function allLive()
    {
        /*Fetch live loan with corresponding bids*/
        $results = $this->loan_model->where('live =', '1')->as_object()->with_bids('where:`accepted`=\'1\'')->as_object()->get_all();
        $loans = array();

        /*
         * Transform stdclass into object
         * (There must be a way to do it automatically with CI, I didn't find it)
         * @TODO : find a better way to do it
         */
        foreach($results as $res){
            $loans[] = Loan::fromStdClass($res);
        }
        return $loans;

    }
}