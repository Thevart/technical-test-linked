<?php
namespace LinkedTest\Model;

use LinkedTest\Model\Bid;


class Loan {

    public $id;
    public $name;
    public $display_name;
    public $amount;
    public $creation_date;
    public $live;
    public $bids;

    public static function fromStdClass($array)
    {
        $loan = new self;
        $loan->id = $array->id;
        $loan->name = $array->name;
        $loan->display_name = $array->display_name;
        $loan->amount = $array->amount;
        $date  = new \DateTime($array->creation_date);
        $loan->creation_date = $date->format('d/m/Y');
        $loan->live = $array->live;

        $bids = array();
        foreach($array->bids as $res){
            $bids[] = Bid::fromStdClass($res);
        }
        $loan->bids = $bids;

        return $loan;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @param mixed $display_name
     */
    public function setDisplayName($display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }

    /**
     * @param mixed $creation_date
     */
    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    /**
     * @return mixed
     */
    public function getLive()
    {
        return $this->live;
    }

    /**
     * @param mixed $live
     */
    public function setLive($live)
    {
        $this->live = $live;
    }



}