<?php

namespace LinkedTest\Model;
/**
 * Created by PhpStorm.
 * User: thevart
 * Date: 31/01/16
 * Time: 12:47
 */

class Bid {
    protected  $id;
    protected  $loan_id;
    protected  $amount;
    protected  $rate;
    protected  $accepted;
    protected  $date;

    /*Transform stdclass retrieved in DB into object*/
    public static function fromStdClass($array)
    {
        $bid = new self;
        $bid->id = $array->id;
        $bid->loan_id = $array->loan_id;
        $bid->amount = $array->amount;
        $bid->rate = $array->rate;
        $bid->accepted = $array->accepted;
        $date  = new \DateTime($array->date );
        $bid->date = $date->format('d/m/Y');
        return $bid;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLoanId()
    {
        return $this->loan_id;
    }

    /**
     * @param mixed $loan_id
     */
    public function setLoanId($loan_id)
    {
        $this->loan_id = $loan_id;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * @param mixed $accepted
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}