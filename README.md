Linked Technical Test
=============
This codeigniter project uses :

 - CodeIgniter 3
 - Twig
 - Foundation 6
 - jQuery
 - jQuery Sorter

Installation
------------
Download the project :
by downloading and extracting the project
	
or by cloning the repo : 

	git clone https://Thevart@bitbucket.org/Thevart/technical-test-linked.git


Create and import the database:

	cd technical-test-linked/
	mysql -u root -p -e "create database marketplace";
	mysql -h localhost -u root -p marketplace < marketplace.sql
Install the dependencies :

	php composer.phar update
Access to the loans page :

	http://localhost/loans


	  

Author
-------
Arthur Thevenet
Website : arthur-thevenet.com
Email : contact@arthur-thevenet.com

